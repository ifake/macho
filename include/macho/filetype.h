/* filetype.h - file type support
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_FILETYPE_H
#define _MACHO_FILETYPE_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>

/* Wrap the header with an include guard */
#include <macho/filetype.tbl>
#include <macho/fileflags.tbl>

__MACHO_BEGIN_CDECL

const char *macho_file_type_to_string (unsigned int bit);
unsigned int macho_file_type_from_string (const char *str);

/**
 * Returns the string name of the flag, indicated by:
 * 
 * <code>1 &lt;&lt; bit</code>
 * 
 * So the bit is in range <code>&lt;0, 31&gt;</code>.
 * 
 * @param bit number of bit of the flag
 * @return flag name, or <code>NULL</code> if flag unknown
 * 
 * @since 0.1.0
 */
const char *macho_file_flag_to_string (unsigned int bit);

/**
 * Returns the number of bit on which is the flag of name <code>str</code>.
 * 
 * @param str flag name
 * @return bit number of bit of the flag, or ((unsigned int) -1), if the flag
 * is unknown
 *
 * @since 0.1.0
 */
unsigned int macho_file_flag_from_string (const char *str);

char *macho_file_flags_to_string (macho_uint32);

__MACHO_END_CDECL

#endif
