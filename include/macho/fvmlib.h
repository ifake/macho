/* fvmlib.h - fixed VM library support
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_FVMLIB_H
#define _MACHO_FVMLIB_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>

typedef struct macho_fvmlib
{
  macho_uint32 name;
  macho_uint32 minor_version;
  macho_uint32 header_addr;
} macho_fvmlib;

#endif
