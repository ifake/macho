/* loadcmd.h - load command support
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MACHO_LOADCMD_H
#define _MACHO_LOADCMD_H

#include <macho/cdefs.h>
#include <macho/inttypes.h>
#include <macho/loadcmd.tbl>
#include <macho/stream.h>

typedef struct macho_lcmd
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
} macho_lcmd;

typedef struct macho_lcmd_segment
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  char segname[16];
  macho_uint32 vmaddr;
  macho_uint32 vmsize;
  macho_uint32 fileoff;
  macho_uint32 filesize;
  macho_uint32 maxprot;
  macho_uint32 initprot;
  macho_uint32 nsects;
  macho_uint32 flags;
} macho_lcmd_segment;

typedef struct macho_lcmd_segment64
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  char segname[16];
  macho_uint64 vmaddr;
  macho_uint64 vmsize;
  macho_uint64 fileoff;
  macho_uint64 filesize;
  macho_uint32 maxprot;
  macho_uint32 initprot;
  macho_uint32 nsects;
  macho_uint32 flags;
} macho_lcmd_segment64;

#include <macho/segflags.tbl>

#include <macho/fvmlib.h>

typedef struct macho_lcmd_fvmlib
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_fvmlib fvmlib;
} macho_lcmd_fvmlib;

#include <macho/dylib.h>

typedef struct macho_lcmd_dylib
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_dylib dylib;
} macho_lcmd_dylib;

typedef struct macho_lcmd_sub_framework
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 umbrella;
} macho_lcmd_sub_framework;

typedef struct macho_lcmd_sub_client
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 client;
} macho_lcmd_sub_client;

typedef struct macho_lcmd_sub_umbrella
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 sub_umbrella;
} macho_lcmd_sub_umbrella;

typedef struct macho_lcmd_sub_library
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 sub_library;
} macho_lcmd_sub_library;

typedef struct macho_lcmd_prebound_dylib
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 name;
  macho_uint32 nmodules;
  macho_uint32 linked_modules;
} macho_lcmd_prebound_dylib;

typedef struct macho_lcmd_dylinker
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 name;
} macho_lcmd_dylinker;

typedef struct macho_lcmd_thread
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 flavor;
  macho_uint32 count;
  macho_uint32 state[];
} macho_lcmd_thread;

typedef struct macho_lcmd_routines
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 init_address;
  macho_uint32 init_module;
  macho_uint32 reserved[6];
} macho_lcmd_routines;

typedef struct macho_lcmd_routines64
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint64 init_address;
  macho_uint64 init_module;
  macho_uint64 reserved[6];
} macho_lcmd_routines64;

typedef struct macho_lcmd_symtab
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 symoff;
  macho_uint32 nsyms;
  macho_uint32 stroff;
  macho_uint32 strsize;
} macho_lcmd_symtab;

typedef struct macho_lcmd_dysymtab
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;

  macho_uint32 ilocalsym;
  macho_uint32 nlocalsym;

  macho_uint32 iextdefsym;
  macho_uint32 nextdefsym;

  macho_uint32 iundefsym;
  macho_uint32 nundefsym;

  macho_uint32 tocoff;
  macho_uint32 ntoc;

  macho_uint32 modtaboff;
  macho_uint32 nmodtab;

  macho_uint32 extrefsymoff;
  macho_uint32 nextrefsyms;;

  macho_uint32 indirectsymoff;
  macho_uint32 nindirectsyms;

  macho_uint32 extreloff;
  macho_uint32 nextrel;

  macho_uint32 locreloff;
  macho_uint32 nlocrel;
} macho_lcmd_dysymtab;

typedef struct macho_lcmd_twolevel_hints
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 offset;
  macho_uint32 nhints;
} macho_lcmd_twolevel_hints;

typedef struct macho_lcmd_prebind_checksum
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 checksum;
} macho_lcmd_prebind_checksum;

typedef struct macho_lcmd_uuid
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint8 uuid[16];
} macho_lcmd_uuid;

typedef struct macho_lcmd_rpath
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 path;
} macho_lcmd_rpath;

typedef struct macho_lcmd_linkedit_data
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 dataoff;
  macho_uint32 datasize;
} macho_lcmd_linkedit_data;

typedef struct macho_lcmd_encryption_info
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 cryptoff;
  macho_uint32 cryptsize;
  macho_uint32 cryptid;
} macho_lcmd_encryption_info;

typedef struct macho_lcmd_encryption_info64
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 cryptoff;
  macho_uint32 cryptsize;
  macho_uint32 cryptid;
  macho_uint32 pad;
} macho_lcmd_encryption_info64;

typedef struct macho_lcmd_version_min
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 version;
  macho_uint32 sdk;
} macho_lcmd_version_min;

typedef struct macho_lcmd_dyld_info
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 rebase_off;
  macho_uint32 rebase_size;
  macho_uint32 bind_off;
  macho_uint32 bind_size;
  macho_uint32 weak_bind_off;
  macho_uint32 weak_bind_size;
  macho_uint32 lazy_bind_off;
  macho_uint32 lazy_bind_size;
  macho_uint32 export_off;
  macho_uint32 export_size;
} macho_lcmd_dyld_info;

typedef struct macho_lcmd_linker_option
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 count;
} macho_lcmd_linker_option;

typedef struct macho_lcmd_symseg
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint32 offset;
  macho_uint32 size;
} macho_lcmd_symseg;

typedef struct macho_lcmd_ident
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
} macho_lcmd_ident;

typedef struct macho_lcmd_fvmfile
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
} macho_lcmd_fvmfile;

typedef struct macho_lcmd_entry_point
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint64 entryoff;
  macho_uint64 stacksize;
} macho_lcmd_entry_point;

typedef struct macho_lcmd_source_version
{
  macho_uint32 cmd;
  macho_uint32 cmdsize;
  macho_uint64 version;
} macho_lcmd_source_version;

__MACHO_BEGIN_CDECL

macho_lcmd *macho_stream_read_lcmd (macho_stream *);
int macho_stream_write_lcmd (macho_stream *, const macho_lcmd *);

const char *macho_lcmd_type_to_string (macho_uint32);
macho_uint32 macho_lcmd_type_from_string (const char *);

__MACHO_END_CDECL

#endif
