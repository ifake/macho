/* cputype.c - CPU type recogniser routine template
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABI
# error DO NOT compile this file directly! Look at cputype-apple.c and \
  cputype-gnu.c for example
#endif

#include <macho/cputype.h>

#include <string.h>

#define CONCAT(a, b) a##b
#define CONCAT_EXPAND(a, b) CONCAT (a, b)

const char *
CONCAT_EXPAND (macho_cputype_to_string_, ABI) (macho_cputype ct)
{
  ct &= 0x01FFFFFF;
#define CPUTYPE(name, code) \
  if (ct == code) \
    { \
      return #name; \
    }
#include INC
#undef CPUTYPE
  return NULL;
}

macho_cputype
CONCAT_EXPAND (macho_cputype_from_string_, ABI) (const char *str)
{
  if (str == NULL)
    return 0;

#define CPUTYPE(name, code) \
  if (strcmp (str, #name) == 0) \
    { \
      return code; \
    }
#include INC
#undef CPUTYPE
  return 0;
}

const char *
CONCAT_EXPAND (macho_cpusubtype_to_string_, ABI) (macho_cputype ct, macho_cpusubtype cst)
{
  ct &= 0x01FFFFFF;
  cst &= 0x00FFFFFF;
#define CPUSUBTYPE(t, st, c) \
  if (ct == MACHO_CPUTYPE_##t && cst == c) \
    return #st;
#include INC_SUB
#undef CPUSUBTYPE
  return NULL;
}

macho_cpusubtype
CONCAT_EXPAND (macho_cpusubtype_from_string_, ABI) (macho_cputype ct, const char *str)
{
  if (str == NULL)
    return 0;

#define CPUSUBTYPE(name, subname, code) \
  if (ct == MACHO_CPUTYPE_##name && strcmp (str, #subname) == 0) \
    { \
      return code; \
    }
#include INC_SUB
#undef CPUSUBTYPE
  return 0; /* Zero is not occupied and most probably will never be */
}
