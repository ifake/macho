/* header.c - Mach-O header input/output
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <macho/header.h>

#define READ32(field) \
  if (macho_stream_read32 (stream, &header->field) != 0) \
    return -1

int
macho_stream_read_header (macho_stream * stream, macho_header * header)
{
  READ32 (magic);
  READ32 (cputype);
  READ32 (cpusubtype);
  READ32 (filetype);
  READ32 (ncmds);
  READ32 (sizeofcmds);
  READ32 (flags);
  return 0;
}

int
macho_stream_read_header64 (macho_stream * stream, macho_header64 * header)
{
  READ32 (magic);
  READ32 (cputype);
  READ32 (cpusubtype);
  READ32 (filetype);
  READ32 (ncmds);
  READ32 (sizeofcmds);
  READ32 (flags);
  READ32 (reserved);
  return 0;
}

int
macho_stream_read_header_fat (macho_stream * stream,
                              macho_header_fat * header)
{
  READ32 (magic);
  READ32 (num);
  return 0;
}

int
macho_stream_read_header_fat_arch (macho_stream * stream,
                                   macho_header_fat_arch * header)
{
  READ32 (cputype);
  READ32 (cpusubtype);
  READ32 (offset);
  READ32 (size);
  READ32 (align);
  return 0;
}

#undef READ32
#define WRITE32(field) \
  if (macho_stream_write32 (stream, header->field) != 0) \
    return -1

int
macho_stream_write_header (macho_stream * stream, const macho_header * header)
{
  WRITE32 (magic);
  WRITE32 (cputype);
  WRITE32 (cpusubtype);
  WRITE32 (filetype);
  WRITE32 (ncmds);
  WRITE32 (sizeofcmds);
  WRITE32 (flags);
  return 0;
}

int
macho_stream_write_header64 (macho_stream * stream,
                             const macho_header64 * header)
{
  WRITE32 (magic);
  WRITE32 (cputype);
  WRITE32 (cpusubtype);
  WRITE32 (filetype);
  WRITE32 (ncmds);
  WRITE32 (sizeofcmds);
  WRITE32 (flags);
  WRITE32 (reserved);
  return 0;
}

int
macho_stream_write_header_fat (macho_stream * stream,
                               const macho_header_fat * header)
{
  WRITE32 (magic);
  WRITE32 (num);
  return 0;
}

int
macho_stream_write_header_fat_arch (macho_stream * stream,
                                    const macho_header_fat_arch * header)
{
  WRITE32 (cputype);
  WRITE32 (cpusubtype);
  WRITE32 (offset);
  WRITE32 (size);
  WRITE32 (align);
  return 0;
}
