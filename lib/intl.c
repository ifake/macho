#include <config.h>

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <locale.h>
#include <stdbool.h>

#include "localedir.h"

static bool init_done = false;

__attribute__ ((__constructor__))
void
macho_intl_init ()
{
  if (init_done)
    return;
  init_done = true;

  bindtextdomain ("macho", LOCALEDIR);
}
