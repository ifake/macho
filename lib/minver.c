#include <config.h>

#include <macho/minver.h>
#include <stdio.h>
#include <stdlib.h>

char *
macho_minver_to_string (macho_minver mv)
{
  char *ret;
  asprintf (&ret, "%u.%u.%u", mv.major, mv.minor, mv.release);
  return ret;
}

macho_minver
macho_minver_extract (macho_uint32 num)
{
  macho_minver ret;

  ret.major = (num & 0xFF0000) >> 16;
  ret.minor = (num & 0xFF00) >> 8;
  ret.release = num & 0xFF;

  return ret;
}

macho_uint32
macho_minver_store (macho_minver mv)
{
  macho_uint32 ret = 0;

  ret |= mv.major;
  ret <<= 8;

  ret |= mv.minor;
  ret <<= 8;

  return ret | mv.release;
}
