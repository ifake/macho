/* dump_header.c - dumps Mach-O header
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <errno.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <macho/filetype.h>
#include <macho/header.h>
#include <macho/stream.h>
#include <stdio.h>
#include <string.h>

#define FAKE_(x) x

static gchar *filename = NULL;

static void
dump (const gchar *prefix, const macho_header *header)
{
  gchar *flagstr = macho_file_flags_to_string (header->flags);
  const gchar *ctstring = macho_cputype_to_string_apple (header->cputype);
  const gchar *cststring = macho_cpusubtype_to_string_apple (header->cputype,
                                                             header->cpusubtype);
  const gchar *ftstring = macho_file_type_to_string (header->filetype);
  g_print (_ ("%sMagic number:                  %0#10x\n"
              "%sCPU type:                      %s (%#x)\n"
              "%sCPU sub-type:                  %s (%#x)\n"
              "%sFile type:                     %s (%#x)\n"
              "%sNumber of load commands:       %u\n"
              "%sTotal size of load commands:   %0#10x\n"
              "%sFlags:                         %s (%0#10x)\n"),
           prefix, header->magic,
           prefix, ctstring, header->cputype,
           prefix, cststring, header->cpusubtype,
           prefix, ftstring, header->filetype,
           prefix, header->ncmds,
           prefix, header->sizeofcmds,
           prefix, flagstr, header->flags);

  g_free (flagstr);
}

static int
do_stream_normal (const gchar *prefix, macho_stream *stream, bool is64)
{
  if (is64)
    {
      macho_header64 hdr;
      if (macho_stream_read_header64 (stream, &hdr) != 0)
        {
          fprintf (stderr, "Could not read header: %s\n", g_strerror (errno));
          return 1;
        }
      dump (prefix, (macho_header *) &hdr);

      g_print (_ ("%sReserved:                      %u\n"),
               prefix, hdr.reserved);
    }
  else
    {
      macho_header hdr;
      if (macho_stream_read_header (stream, &hdr) != 0)
        {
          fprintf (stderr, "Could not read header: %s\n", g_strerror (errno));
          return 1;
        }
      dump (prefix, &hdr);
    }

  return 0;
}

static int do_stream (const gchar *prefix, macho_stream *stream);

static int
do_stream_fat (const gchar *prefix, macho_stream *stream)
{
  macho_header_fat hdr;
  macho_header_fat_arch arch;

  gchar *prefix_sub = g_strjoin (prefix, "\t", NULL);
  if (macho_stream_read_header_fat (stream, &hdr) != 0)
    {
      fprintf (stderr, "Could not read header: %s\n", g_strerror (errno));
      g_free (prefix_sub);
      return 1;
    }
  g_print (_ ("Magic number:                  %0#10x\n"), hdr.magic);
  g_print (_ ("%u architectures:\n"), hdr.num);
  macho_uint32 i = 0;
  while (i < hdr.num)
    {
      g_print (_ ("Architecture %u:\n"), i);
      if (macho_stream_read_header_fat_arch (stream, &arch) != 0)
        {
          fprintf (stderr, "Could not read architecture: %s\n", g_strerror
                   (errno));
          g_free (prefix_sub);
          return 1;
        }
      g_print (_ ("%s\tOffset:                        %0#10x\n"
                  "%s\tSize:                          %0#10x\n"
                  "%s\tAlignment:                     %0#10x\n"),
               prefix, arch.offset,
               prefix, arch.size,
               prefix, arch.align);

      macho_stream *arch_stream = macho_stream_open_offset (filename, "r",
                                                            arch.offset);

      if (arch_stream == NULL)
        {
          fprintf (stderr, "Could not open sub-stream: %s\n", g_strerror
                   (errno));
          g_free (prefix_sub);
          return 1;
        }

      int i2;
      if ((i2 = do_stream (prefix_sub, arch_stream)) != 0)
        {
          macho_stream_close (arch_stream);
          return i2;
        }

      macho_stream_close (arch_stream);

      i++;
    }
  g_free (prefix_sub);

  return 0;
}

static int
do_stream (const gchar *prefix, macho_stream *stream)
{
  macho_uint32 magic;

  if (macho_stream_read32 (stream, &magic) != 0)
    {
      fprintf (stderr, "Could not read magic number: %s\n", g_strerror
               (errno));
      return 1;
    }

  if (macho_stream_seek (stream, MACHO_STREAM_SEEK_CURRENT, -4) != 0)
    {
      fprintf (stderr, "Could not seek: %s\n", g_strerror (errno));
      return 1;
    }

  switch (magic)
    {
    case MACHO_HEADER_MAGIC_32:
    case MACHO_HEADER_MAGIC_64:
      return do_stream_normal (prefix, stream, magic == MACHO_HEADER_MAGIC_64);
    case MACHO_HEADER_MAGIC_FAT:
      return do_stream_fat (prefix, stream);
    default:
      fprintf (stderr, "Unrecognized file!\n");
      return 1;
    }
}

int
dump_header (gchar ** args)
{
  GError *error = NULL;
  gchar **filenames = NULL;

  static gchar *arch = NULL;

  const GOptionEntry ENTRIES[] = {
    {"arch", 'a', 0, G_OPTION_ARG_STRING, &arch,
      FAKE_ ("Dumps only the selected architecture of a fat file"), FAKE_ ("<ARCHITECTURE>")},
    {"", 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &filenames, NULL, FAKE_ ("<FILE>")},
    {NULL, 0, 0, 0, NULL, NULL, NULL}
  };

  g_set_prgname ("macho dump_header");

  GOptionContext *optctx = g_option_context_new (_("- Dump information from "
                                                   "Mach-O header"));

  g_option_context_add_main_entries (optctx, ENTRIES, "macho");

  if (!g_option_context_parse_strv (optctx, &args, &error))
    {
      fprintf (stderr, _("Option parsing error: [%04u] %s\n"), error->code,
               error->message);
    }

  if (filenames == NULL || filenames[0] == NULL)
    {
      fprintf (stderr, _("Usage: macho dump_header [OPTION...] <FILE>\n"
                         "Run with --help for more information\n"));
      return 2;
    }
  else if (filenames[1] != NULL)
    {
      fprintf (stderr, _("Warning: More than one filename passed - "
                         "ignoring\n"));
    }

  filename = filenames[0];

  macho_stream *stream = macho_stream_open (filename, "r");

  if (stream == NULL)
    {
      fprintf (stderr, "Could not open %s: %s\n", filename, g_strerror
               (errno));
      return 1;
    }

  int ret = do_stream ("", stream);

  macho_stream_close (stream);

  return ret;
}
