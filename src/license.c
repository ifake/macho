/* license.c - Prints the license of this program
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <macho/header.h>
#include <macho/stream.h>

#include "dirs.h"
#include "gpl.c"

int
license (gchar ** args)
{
  GError *error = NULL;

  g_set_prgname ("macho license");

  GOptionContext *optctx = g_option_context_new (_("- Print GPL"));

  if (!g_option_context_parse_strv (optctx, &args, &error))
    {
      fprintf (stderr, _("Option parsing error: [%04u] %s\n"), error->code,
               error->message);
      return 2;
    }

  g_print ("%s\n", GPL);

  return 0;
}
