/* macho.c - Main utility
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <glib.h>
#include <glib/gi18n-lib.h>
#ifdef HAVE_GMODULE
#include <gmodule.h>
#endif
#include <locale.h>
#include <macho/filetype.h>
#include <macho/header.h>
#include <macho/intl.h>
#include <macho/stream.h>
#include <stdio.h>
#include <stdlib.h>

#include "dirs.h"

static bool set_app_name_called = false;

typedef int (*CommandCallback) (gchar ** args);

static void init_commands ();
static CommandCallback lookup_command (const gchar * name);

char *alias = NULL;

#define FAKE_(x) x

int
main (gint argc, gchar ** argv)
{
  g_set_prgname ("macho");
  if (!set_app_name_called)
    {
      set_app_name_called = true;
      g_set_application_name ("MachO");
    }

  alias = argv[0];

#if defined (ENABLE_NLS) && ENABLE_NLS
  setlocale (LC_ALL, "");
  macho_intl_init ();
#endif

  init_commands ();

  (void) argc;

  GError *error = NULL;
  GOptionContext *optctx;
  gchar **args;

#ifdef G_OS_WIN32
  args = g_win32_get_command_line ();
#else
  args = g_strdupv (argv);
#endif

  optctx = g_option_context_new (FAKE_ ("<COMMAND> [COMMAND OPTION...] - "
                                        "Mach-O multi-tool"));
  g_option_context_set_summary (optctx,
                                FAKE_ ("Operates on Mach-O files in various "
                                       "ways"));

  g_option_context_set_description (optctx,
                                    FAKE_
                                    ("Each built-in command also carries a help, try:\n"
                                     "    macho <command> --help\n"
                                     "to see it.\n" "\n"
                                     "If the system provides a way, MachO will also look for commands\n"
                                     "in a directory, use command \"dump_commanddir\" to check the \n"
                                     "location. To see the list of built-in commands, use the \n"
                                     "\"list_builtin\" command.\n"));

  g_option_context_set_translation_domain (optctx, "macho");
  g_option_context_set_strict_posix (optctx, true);

  if (!g_option_context_parse_strv (optctx, &args, &error))
    {
      fprintf (stderr, _("Option parsing error: [%04u] %s\n"), error->code,
               error->message);
      return 2;
    }

  g_option_context_free (optctx);

  if (args[1] == NULL)
    {
      args[0] = "shell";
      return lookup_command ("shell") (args);
    }
  else
    {
      return lookup_command (args[1]) (args + 1);
    }
}

static GHashTable *command_hash = NULL;

extern int dump_header (gchar **);
extern int dump_commanddir (gchar **);
extern int dump_lcmds (gchar **);
extern int shell (gchar **);
extern int license (gchar **);
static int list_builtin (gchar **);
static int help (gchar **);

static void
init_commands ()
{
  command_hash = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_insert (command_hash, "dump_header", dump_header);
  g_hash_table_insert (command_hash, "list_builtin", list_builtin);
  g_hash_table_insert (command_hash, "dump_commanddir", dump_commanddir);
  g_hash_table_insert (command_hash, "shell", shell);
  g_hash_table_insert (command_hash, "help", help);
  g_hash_table_insert (command_hash, "license", license);
  g_hash_table_insert (command_hash, "dump_lcmds", dump_lcmds);
}

static int
command_not_found (gchar ** args)
{
  fprintf (stderr, _("Command not found: %s\n"), args[0]);
  return 2;
}

static void
list_builtin_ITERATE (gpointer key, gpointer value, gpointer userdata)
{
  (void) value;
  (void) userdata;

  g_print ("%s\n", (const char *) key);
}

static int
help (gchar ** args)
{
  GError *error = NULL;

  g_set_prgname ("macho help");

  GOptionContext *optctx =
    g_option_context_new (_("- Print the help text"));

  if (!g_option_context_parse_strv (optctx, &args, &error))
    {
      fprintf (stderr, _("Option parsing error: [%04u] %s\n"), error->code,
               error->message);
      return 2;
    }

  gchar *new_args[] = {
    alias,
    "--help",
    NULL
  };
  return main (2, new_args);
}

static int
list_builtin (gchar ** args)
{
  GError *error = NULL;

  g_set_prgname ("macho list_builtin");

  GOptionContext *optctx =
    g_option_context_new (_("- List built-in commands"));

  if (!g_option_context_parse_strv (optctx, &args, &error))
    {
      fprintf (stderr, _("Option parsing error: [%04u] %s\n"), error->code,
               error->message);
      return 2;
    }

  g_hash_table_foreach (command_hash, list_builtin_ITERATE, NULL);

  return 0;
}

static CommandCallback
lookup_command (const gchar * name)
{
  CommandCallback ret = NULL;

  {
    ret = g_hash_table_lookup (command_hash, name);

    if (ret != NULL)
      goto before_return;
  }
#ifdef HAVE_GMODULE
  if (g_module_supported ())
    {
      gchar *path = g_module_build_path (COMMANDDIR, name);
      if (path)
        {
          GModule *module = g_module_open (path, G_MODULE_BIND_LAZY |
                                           G_MODULE_BIND_LOCAL);
          if (module)
            {
              if (g_module_symbol (module, name, (gpointer *) & ret))
                {
                  g_module_make_resident (module);
                  g_free (path);
                  goto before_return;
                }
              else
                {
                  ret = NULL;
                }

              g_module_close (module);
            }

          g_free (path);
        }
    }
#endif

before_return:
  if (ret == NULL)
    ret = command_not_found;
  return ret;
}
